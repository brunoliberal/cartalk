import { Plate } from "../domain/plate";

export class Message {
  text: String;
  date: Date;
  plate: Plate;
}

