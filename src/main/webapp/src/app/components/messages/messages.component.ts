import { Component, OnInit } from "@angular/core";

import { ActivatedRoute, Params } from '@angular/router';
import { Message } from "../../domain/message";
import { MessageService } from "../../services/message.service";
import {MdSnackBar} from '@angular/material';


@Component({
  selector: "messages",
  templateUrl: "./messages.component.html",
  styleUrls: ["./messages.component.css"]
})

export class MessagesComponent implements OnInit {
  title: String;
  plateNumber: String;
  messages: Message[];
  errorMessage: string;
  newMessage: boolean;
  
   
  constructor(
    public messageService: MessageService,
    public route: ActivatedRoute,
    public snackBar: MdSnackBar
  ) {}
  
  ngOnInit(): void {
 
    this.route.params.subscribe(params => {
      this.plateNumber = params['id'];
      
      }
    );
    
   this.route
      .queryParams
      .subscribe(params => {
        this.newMessage = params['newMessage'];

      });
    
    this.title =  this.plateNumber  + " Messages";

    this.messageService.getAllMessagesPlates(this.plateNumber).subscribe(
    messages => this.messages = messages, error =>  this.errorMessage = <any>error);
        
    if(this.newMessage) {
      this.openSnackBar("Your message has been sent!", "")
    }
      
  }
  
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
  
}
