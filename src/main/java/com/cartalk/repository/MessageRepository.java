package com.cartalk.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.cartalk.model.Message;

@Repository
public class MessageRepository {

	@PersistenceContext 
	private EntityManager em;
	
	public List<Message> findByPlateNumber(String plateNumber) {
		Query query = em.createQuery("select distinct m from Message m join  m.plate p where p.plateNumber = :plateNumber order by m.date");
		query.setParameter("plateNumber", plateNumber);
	
		return query.getResultList();
	}
	
	public Message save(Message message) {
		em.persist(message);
		return message;
	}

}
