import { NgModule }      from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule, JsonpModule }    from "@angular/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "@angular/material";
import { RouterModule, Routes }   from "@angular/router";

import { AppComponent }  from "./app.component";
import { MenuComponent }  from "./components/menu/menu.component";
import { SearchPlateComponent }  from "./components/search-plate/search-plate.component";
import { MessagesComponent }  from "./components/messages/messages.component";
import { CrudComponent }  from "./components/crud/crud.component";
import { SendMessageComponent }  from "./components/send-message/send-message.component";


import { TextMaskModule } from 'angular2-text-mask';


const routes: Routes = [
  { path: "", redirectTo: "/searchPlate", pathMatch: "full" },
  { path: "searchPlate",  component: SearchPlateComponent },
  { path: ":id/messages", component: MessagesComponent },
  { path: "crud",     component: CrudComponent },
  { path: ":id/sendmessage", component: SendMessageComponent }
];

@NgModule({
  imports:      [
    BrowserModule,
    HttpModule,
    JsonpModule,
    BrowserAnimationsModule,
    MaterialModule,
    RouterModule.forRoot(routes),
    TextMaskModule
   ],
  declarations: [
    AppComponent,
    MenuComponent,
    SearchPlateComponent,
    MessagesComponent,
    CrudComponent,
    SendMessageComponent
   ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
