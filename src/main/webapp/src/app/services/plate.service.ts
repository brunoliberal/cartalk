import { Injectable } from "@angular/core";
import { Response, Http, Headers, RequestOptions } from "@angular/http";

import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";

import { Plate } from "../domain/plate";
import { Message } from "../domain/message";

@Injectable()
export class PlateService {
  private platesUrl = "http://localhost:8080/api/plate";

  constructor(private http: Http) {}
  cretePlate(plateNumber: String): Observable<Plate> {
    let plate = new Plate;
    plate.plateNumber = plateNumber;

    let headers = new Headers({ "Content-Type": "application/json" });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.platesUrl, plate, options).map(res => res.json()).catch(this.handleError);
  }

  getPlates(): Observable<Plate[]> {
    return this.http.get(this.platesUrl).map(res => res.json()).catch(this.handleError);
  }

  getMessagesByPlateNumber(plateNumber: String): Observable<Message[]> {
    return this.http.get(this.platesUrl + "/" + plateNumber + "/messages").map(res => res.json()).catch(this.handleError);
  }

  private handleError(error: Response | any) {
    let errMsg: string = "An error occurred!!!";
    console.error(errMsg, error);
    return Observable.throw(errMsg);
  }
}
