import { Component, OnInit } from "@angular/core";

import { PlateService } from "../../services/plate.service";
import { Plate } from "../../domain/plate";

@Component({
  selector: "crud",
  templateUrl: "./crud.component.html",
  styleUrls: ["./crud.component.css"]
})

export class CrudComponent implements OnInit {
  title: String = "CRUD";
  errorMessage: string;
  plates: Plate[];

  constructor(private plateService: PlateService) {}

  ngOnInit(): void {
    this.plateService.getPlates().subscribe(
      plates => this.plates = plates, error =>  this.errorMessage = <any>error);
  }

  onCreatePlate(plateNumber: string): void {
    if (!plateNumber) {
      return;
    }
    
    this.plateService.cretePlate(plateNumber).subscribe(
      plate => this.plates.push(plate), error =>  this.errorMessage = <any>error);
  }
}
