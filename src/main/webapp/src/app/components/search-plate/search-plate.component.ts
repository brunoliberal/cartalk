import { Component, OnInit} from "@angular/core";
import { Router }   from "@angular/router";

@Component({
  selector: "search-plate",
  templateUrl: "./search-plate.component.html",
  styleUrls: ["./search-plate.component.css"]
})

export class SearchPlateComponent {
  title: String = "CarTalk";

  public mask = [/[a-zA-Z]/ , /[a-zA-Z]/, /[a-zA-Z]/, '-', /\d/, /\d/, /\d/, /\d/]
  

  constructor(
    public router: Router
  ) {}

  onSearchPlate(plateNumber: String) {
    if (!plateNumber) {
      return;
    }
    
    
    this.router.navigateByUrl("/" + plateNumber.toUpperCase() + "/messages");
  }
}
