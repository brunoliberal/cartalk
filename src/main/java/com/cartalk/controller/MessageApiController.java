package com.cartalk.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cartalk.model.Message;
import com.cartalk.service.MessageService;

@RestController
@RequestMapping("api/message")
public class MessageApiController { 

  @Autowired
  private MessageService messageService;

  @RequestMapping(value = "/plate/{plateNumber}", method = RequestMethod.GET)
  public ResponseEntity<List<Message>> getMessagesByPlate(@PathVariable String plateNumber) {
    return new ResponseEntity<List<Message>>(messageService.getAllMessages(plateNumber), HttpStatus.OK);
  }
  
  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity<Message> save(@RequestBody Message message) {
    return new ResponseEntity<Message>(messageService.save(message), HttpStatus.OK);
  }

}
