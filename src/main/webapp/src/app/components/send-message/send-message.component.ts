import { Component, OnInit } from "@angular/core";
import { Router }   from "@angular/router";
import { ActivatedRoute, Params } from '@angular/router';
import { MessageService } from "../../services/message.service";

@Component({
  selector: "sendMessage",
  templateUrl: "./send-message.component.html",
  styleUrls: ["./send-message.component.css"]
})

export class SendMessageComponent implements OnInit {
  title: String = "Send Message";
  errorMessage: string;
  plateNumber: String;
  
  constructor(private messageService: MessageService,
    private router: Router, private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.plateNumber = params['id'];
      }
    );
        

  }
  
  onSendMessage(message: string): void {
    this.messageService.saveMessage(this.plateNumber, message).subscribe(
      message => this.router.navigate(["/" + this.plateNumber.toUpperCase() + "/messages"] , { queryParams: { newMessage: true }} )
      
    , error =>  this.errorMessage = <any>error);
    
  }
  
 
 
 }
