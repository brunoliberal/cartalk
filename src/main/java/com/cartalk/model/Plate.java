package com.cartalk.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Plate implements Serializable {

  private static final long serialVersionUID = -5464953722355694187L;

  public static final String PLATE_NUMBER_REGEX = "^[a-zA-Z]{3}-[0-9]{4}$";

  @Id 
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(columnDefinition = "serial")
  private Long id;

  @Column(unique = true)
  private String plateNumber;

  @JsonManagedReference
  @OneToMany(mappedBy = "plate")
  private Set<Message> messages;

  public Plate() {
  }

  /**
   * Default constructor.
   */
  public Plate(Long id, String number) {
    this.id = id;
    this.plateNumber = number;
  }

  public Plate(String number) {
    this.plateNumber = number;
  }

  public Long getId() {
    return id;
  }

  public Set<Message> getMessages() {
    return messages;
  }

  public void setMessages(Set<Message> messages) {
    this.messages = messages;
  }

	public String getPlateNumber() {
		return plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}  
  

}
