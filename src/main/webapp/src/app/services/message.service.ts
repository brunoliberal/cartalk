import { Injectable } from "@angular/core";
import { Response, Http, Headers, RequestOptions } from "@angular/http";

import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";

import { Message } from "../domain/message";
import { Plate } from "../domain/plate";


@Injectable()
export class MessageService {
  private messagesUrl = "http://localhost:8080/api/message";
  
  constructor(private http: Http) {}

  getAllMessagesPlates(plateNumber: String): Observable<Message[]> {      
   
    return this.http.get(this.messagesUrl + "/plate/" + plateNumber)
      .map(res => res.json()).catch(this.handleError);
  }
  
  saveMessage(plateNumber: String, messageContent: String): Observable<Message> {
    let message = new Message;
    message.plate = new Plate;
    message.text = messageContent;
    
    message.plate.plateNumber = plateNumber;

    let headers = new Headers({ "Content-Type": "application/json" });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.messagesUrl, message, options).map(res => res.json()).catch(this.handleError);
  }

  private handleError(error: Response | any) {
    let errMsg: string = "An error occurred!!!";
    console.error(errMsg, error);
    return Observable.throw(errMsg);
  }

}
