package com.cartalk.service;

import com.cartalk.exception.BusinessException;
import com.cartalk.model.Message;
import com.cartalk.model.Plate;
import com.cartalk.repository.PlateRepository;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlateService {

  @Autowired
  private PlateRepository plateDao;
  
  /**
   * Get messages by plate number.
   * 
   * @param number
   *          plate number
   * @return list of messages.
   */
  public Set<Message> getMessagesByPlateNumber(String number) {
    if (number == null || !number.matches(Plate.PLATE_NUMBER_REGEX)) {
      throw new BusinessException("Bad params");
    }
    Plate plate = plateDao.findOneByPlateNumber(number);
    if (plate == null) {
      plate = plateDao.save(new Plate(number));
    }
    return plate.getMessages();
  }

  public Plate get(long id) {
    return plateDao.findOne(id);
  }

  public Plate save(Plate plate) {
    return plateDao.save(plate);
  }

  public Iterable<Plate> findAll() {
    return plateDao.findAll();
  }
  
  public Plate findPlateByNumber(String plateNumber) {
  	return plateDao.findOneByPlateNumber(plateNumber);
    
  }
}
