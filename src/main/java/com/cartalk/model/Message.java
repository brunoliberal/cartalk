package com.cartalk.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Message implements Serializable {

  private static final long serialVersionUID = 4582995499224695003L;

  @Id 
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(columnDefinition = "serial")  
  private Long id;
  
  private String text;

  @JsonBackReference
  @ManyToOne(fetch = FetchType.LAZY)
  private Plate plate;
  
  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Calendar date;

  public Message() {
  }

  /**
   * Default constructor using fields.
   */
  public Message(Long id, String text, Plate plate) {
    this.id = id;
    this.text = text;
    this.plate = plate;
  }

  /**
   * TODO: for enpoints only. remove later.
   */
  public Message(Long id, String text, String plate) {
    this.id = id;
    this.text = text;
    this.plate = new Plate((long) Math.random(), plate);
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Plate getPlate() {
    return plate;
  }

  public void setPlate(Plate plate) {
    this.plate = plate;
  }
  
  public Calendar getDate() {
    return date;
  }

  public void setDate(Calendar date) {
    this.date = date;
  }

}
