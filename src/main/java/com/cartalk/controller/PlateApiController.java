package com.cartalk.controller;

import com.cartalk.model.Message;
import com.cartalk.model.Plate;
import com.cartalk.service.PlateService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api/plate")
public class PlateApiController { 

  @Autowired
  private PlateService plateService;

  @RequestMapping(value = "/test", method = RequestMethod.GET)
  public String test() {
    return "Yes, your endpoint is working!";
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public ResponseEntity<Plate> get(@PathVariable long id) {
    return new ResponseEntity<Plate>(plateService.get(id), HttpStatus.OK);
  }

  @RequestMapping(method = RequestMethod.GET)
  public ResponseEntity<Iterable<Plate>> list() {
    return new ResponseEntity<Iterable<Plate>>(plateService.findAll(), HttpStatus.OK);
  }

  @RequestMapping(value = "/{number}/messages", method = RequestMethod.GET)
  public ResponseEntity<Iterable<Message>> getMessagesByPlateNumber(@PathVariable String number) {
    return new ResponseEntity<Iterable<Message>>(plateService.getMessagesByPlateNumber(number),
        HttpStatus.OK);
  }
}
