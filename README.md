# Car Talk App

### Objective ###
Create a web app focused on mobile browser that will let car owners exchange messages based on their plate's number.

## Stories ##
- search messages by plate
- send a message for a plate

## Technologies ##

### Backend ###
- Spring Boot
- Postgres Database

### Frontend ###
- AngularJS
- Material
