package com.cartalk.service;

import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cartalk.model.Message;
import com.cartalk.model.Plate;
import com.cartalk.repository.MessageRepository;

@Service
public class MessageService {

  @Autowired
  private MessageRepository messageRepository;
  
  @Autowired
  private PlateService plateService;
  
  public List<Message> getAllMessages(String plateNumber) {  	
  	return messageRepository.findByPlateNumber(plateNumber);
  }
  
  @Transactional
  public Message save(Message message) {
  	Plate plate = plateService.findPlateByNumber(message.getPlate().getPlateNumber());
  	
  	if (plate == null) {
      plate = plateService.save(new Plate(message.getPlate().getPlateNumber()));
    }
  	
  	message.setPlate(plate);
  	message.setDate(Calendar.getInstance());
  	
    return messageRepository.save(message);
  }
  
}
