package com.cartalk.repository;

import com.cartalk.model.Plate;

import org.springframework.data.repository.CrudRepository;

public interface PlateRepository extends CrudRepository<Plate, Long> {

  Plate findOneByPlateNumber(String plateNumber);
}
