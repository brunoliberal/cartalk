truncate message cascade;

truncate plate cascade;

insert into plate (plate_number) values ('HKC-6811');

insert into message(date, text, plate_id) values ('2017-06-10 12:21:13', 'I saw you parked in a wrong place.', 
	(select id from plate where plate_number = 'HKC-6811') );

insert into message(date, text, plate_id) values ('2017-06-11 10:15:13' , 'Hi, my name is Tom, I had an emergency, sorry for that.', 
	(select id from plate where plate_number = 'HKC-6811'));

insert into plate (plate_number) values ('HKD-1578');

insert into message(date, text, plate_id) values ('2017-05-12 10:20:13', 'Ow. I saw you driving. You look gorgeus! Call me 9892-8762', 
	(select id from plate where plate_number = 'HKD-1578'));

insert into message(date, text, plate_id) values ('2017-05-13 09:20:13', 'I am married!', (select id from plate where plate_number = 'HKD-1578'));

commit;

