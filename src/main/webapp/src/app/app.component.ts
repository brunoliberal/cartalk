import { Component } from "@angular/core";

import { PlateService } from "./services/plate.service";
import { MessageService } from "./services/message.service";


@Component({
  selector: "my-app",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
  providers: [ PlateService, MessageService ]
})

export class AppComponent {
  tittle: string = "CarTalk";
}
